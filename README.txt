**Problem:**  
Lukus is looking for a new apartment to live in.  Lukus loves 2 things in life more than anything else...
his pet cat 'Fluffy' and living in a high rise condo.  The higher the better.  Lukus also enjoys leaving the
window open on occasion, and he is a little nervous about Fluffy jumping out the window and plummeting to his 
death.  Our challenge is to find the highest floor that Lukus can live on with Fluffy and he can still sleep 
easy because if Fluffy does jump out the window one day, he won't die (from the fall anyway).

**How we're going to find the answer:**  
Lucky for us, Denver is currently under a stray cat infestation.  Before looking at each new high rise condo,
Lukus and his assistant (in this case, you) will round up a number of stray cats to test with.  The buildings 
that you're looking at will vary from 10 floors up to 1000 floors (yeah, we know this is ridiculous, but so is 
the question in general).  The number of stray cats that you're able to round up before each visit will vary as 
well, you'll be able to find at least 2 but up to possibly 10.  Different buildings will have a DIFFERENT max 
floor that cats can survive when thrown from (due to wind factors, grass vs. concrete, etc) so each building
will need to be tested individually and you can't use previous results from other buildings.

**Assumptions:**
1) All cats are created equally.  If a stray cat lives/dies when thrown from a specific floor, Fluffy will 
live/die if he were to jump from the same floor.
2) If a cat lives when thrown from a floor, it will live when thrown from ANY floor BELOW that.
3) If a cat dies when thrown from a floor, it will die when thrown from ANY floor ABOVE that.
4) If a stray cat survives the toss, you can round up and re-use that same cat in a future toss.
5) In this extremely crude example, we're not worried about the lives of these stray cats.  We're more
focused on optimizing our strategy by performing the least number of cat tosses to get to an answer.

**Coding Challenge:**
The first part of this challenge is pulling down the project and getting it to compile.

You must then write an application that when given the inputs (a high rise condo object, and a number of cats), 
can determine maxLiveableFloor while doing as few cat tosses as possible.  Feel free to use the provided
template, or create your own if you see fit.

Your console output might look like:
Console.WriteLine("I can find the best floor with 100% certainty using only {0} tries with my first attempt on
{1} floor", leastTries, bestFloorToStart);

**Hints:**
1) An optimal solution is fantastic, but we're much more concerned about the approach to the problem and the
thoroughness of handling all cases thrown at you.
2) It can be very helpful to build in a 'test' output... so have the 'maxLivableFloor' be an input in your
test code and spit out the steps and results - I.E.:
  Step 1: Attempted Floor 21 - Result Cat Lived (3 cats remaining)
  Step 2: Attempted Floor 28 - Result Cat Died (2 cats remaining)
  Step 3: Attempted Floor 25 - Result Cat Lived (2 cats remaining)
  Step 4: Attempted Floor 26 - Result Cat Died (1 cat remaining)
  Floor 25 is maxLivableFloor - Found in 4 attempts
3) If you were given 2 cats and 100 stories, in a worst case scenario you could find maxLiveableFloor in 14 tosses.

asdfasdfasdf