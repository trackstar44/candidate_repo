﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatQuestion
{
    class Program
    {
        static void Main(string[] args)
        {
            HighRiseCondo condo = new HighRiseCondo();
            int numCats = new Random((int)DateTime.Now.Ticks).Next(2, 10);
            Solution s = new Solution();
            s.solution(condo, numCats);
            Console.WriteLine("\n" + condo.ToString());
            Console.ReadLine();
        }
    }
}
