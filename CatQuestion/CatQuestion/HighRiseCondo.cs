﻿using System;

namespace CatQuestion
{
    class HighRiseCondo
    {
        public readonly int stories;
        public readonly int maxLivableFloor;


        readonly int MAX_FLOORS = 1000;
        readonly int MIN_FlOORS = 10;

        public HighRiseCondo()
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            stories = r.Next(MIN_FlOORS, MAX_FLOORS);
            maxLivableFloor = r.Next(MIN_FlOORS, stories);
        }

        override
        public string ToString()
        {
            return "Number of stories: " + stories + "\nMax liveable floors: " + maxLivableFloor;
        }
    }
}
