﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatQuestion
{
    class Solution
    {
        public void solution(HighRiseCondo condo, int numCats)
        {
            // TODO your solution will go here.  Use whatever libraries you need.
        }

        // returns true if it dies.  False if it lives.
        public bool seeIfCatDies(HighRiseCondo condo, int floorAttempted)
        {
            return floorAttempted <= condo.maxLivableFloor;
        }
    }
}
